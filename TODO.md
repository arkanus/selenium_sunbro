ToDo
====

* setup.py (packaging)
* tests
* Python3 support
* tox.ini (multiple version testing)
* Dedicated finder classes for each selenium find_element_* methods
* Integration with nose-selenium
